using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Collider : MonoBehaviour
{
    public Transform head;
    public Transform offSetHead;
    public Transform player;
    public float distanceMax = 4;
    public float offSetVertical;
    public Vector3 offsetFinal;
    public float offsetDistance;
    public RaycastHit camHit;
    

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);

    }

    void Update(){
        Vector3 local = new Vector3(0, 0, -1) * distanceMax;
        Vector3 global = offSetHead.TransformPoint(local);
        Vector3 vector3 = new Vector3(global.x, offSetHead.position.y + Mathf.Tan(Mathf.Deg2Rad * (head.eulerAngles.x + offSetVertical)) * (distanceMax) * player.localScale.x, global.z);
        
        vector3 += player.TransformDirection(offsetFinal).normalized * offsetDistance;
        Vector3 norma = Vector3.Normalize(vector3 - offSetHead.position);
        Vector3 pos = offSetHead.position + norma*distanceMax* player.localScale.x;
        if (Physics.Linecast(offSetHead.position, pos, out camHit))
        {  
            float distance = Vector3.Distance(offSetHead.position, camHit.point);
            pos = offSetHead.position + norma * (distance - 0.2f);
        }
        transform.position = pos;
        transform.LookAt(head.position + player.TransformDirection(offsetFinal).normalized * offsetDistance);
        transform.rotation = Quaternion.Euler(head.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
    }
    void OnDrawGizmos()
    {
        
        Vector3 local = new Vector3(0, 0, -1) * distanceMax;
        Vector3 global = offSetHead.TransformPoint(local);
        Vector3 vector3 = new Vector3(global.x, offSetHead.position.y + Mathf.Tan(Mathf.Deg2Rad * (head.eulerAngles.x + offSetVertical)) * (distanceMax) * player.localScale.x, global.z);
        
        vector3 += player.TransformDirection(offsetFinal).normalized * offsetDistance;
        Vector3 norma = Vector3.Normalize(vector3 - offSetHead.position);
        Vector3 pos = offSetHead.position + norma*distanceMax* player.localScale.x;
        
        Gizmos.color = Color.white;
        Gizmos.DrawLine(offSetHead.position, pos);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(offSetHead.position, global);
        Gizmos.DrawSphere(pos, 0.1f);
        if (Physics.Linecast(offSetHead.position, pos, out camHit))
        {  
            float distance = Vector3.Distance(offSetHead.position, camHit.point);
            pos = offSetHead.position + norma * (distance - 0.2f);
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(offSetHead.position, pos);
            Gizmos.DrawSphere(pos, 0.1f);
        }

    }
    public float getOffSetDistance()
    {
        return offsetDistance;
    }
    public void setOffSetDistance(float offSetDistance)
    {
        this.offsetDistance = offSetDistance;
    }
}
