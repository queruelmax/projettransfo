using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Menu_HighScore : MonoBehaviour
{

    public TextMeshProUGUI FirstHS;
    public TextMeshProUGUI SecondHS;
    public TextMeshProUGUI ThirdHS;

    [SerializeField] private Save save;

    void Start()
    {
        if (save.GetHS()[0] > 0){

            int seconds1 = (int)save.GetHS()[0] % 60;
            int minutes1 = ((int)save.GetHS()[0] / 60) % 60;
            int milliseconde = (int)((save.GetHS()[0] - (int)(save.GetHS()[0]))*1000);

            FirstHS.text = "First Level => " + minutes1 + ":" + (seconds1 <= 9 ? "0" : "") + seconds1 + ","
                + (milliseconde <= 99 ? "0" : "") + (milliseconde <= 9 ? "0" : "") + milliseconde;
        } else {
            FirstHS.text = "First Level => Not Played";
        }
        if (save.GetHS()[1] > 0){
            int seconds2 = (int)save.GetHS()[1] % 60;
            int minutes2 = ((int)save.GetHS()[1] / 60) % 60;
            int milliseconde = (int)((save.GetHS()[1] - (int)(save.GetHS()[1])) * 1000);
            SecondHS.text = "Second Level => " + minutes2 + ":" + (seconds2 <= 9 ? "0" : "") + seconds2 + "," 
                + (milliseconde <= 99 ? "0" : "") + (milliseconde <= 9 ? "0" : "") +   milliseconde;
        } else {
            SecondHS.text = "Second Level => Not Played";
        }
        if (save.GetHS()[2] > 0){
            int seconds3 = (int)save.GetHS()[2] % 60;
            int minutes3 = ((int)save.GetHS()[2] / 60) % 60;
            int milliseconde = (int)((save.GetHS()[2] - (int)(save.GetHS()[2])) * 1000);
            ThirdHS.text = "Third Level => " + minutes3 + ":" + (seconds3 <= 9 ? "0" : "") + seconds3 + ","
                + (milliseconde <= 99 ? "0" : "") + (milliseconde <= 9 ? "0" : "") + milliseconde;
        } else {
            ThirdHS.text = "Third Level => Not Played";
        }
    }

}
