using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Menu_Manager : MonoBehaviour
{

    public GameObject MenuPause;
    public GameObject MenuDeath;
    public GameObject MenuEnd;
    public GameObject MenuGameEnd;
    public GameObject MenuLevel;
    public TextMeshProUGUI Timer;

    private Key_Manager kManager;
    [SerializeField] private Death_Manager dManager;
    [SerializeField] private Level_Manager lManager;
    [SerializeField] private Transforms_Manager tManager;
    [SerializeField] private Chrono chrono;
    [SerializeField] private Portal portalEnd;
    [SerializeField] private Save save;
    private bool callDead = false;

    public void Awake()
    {
        Time.timeScale = 1.0f;
        MenuPause.SetActive(false);
        MenuDeath.SetActive(false);
        MenuEnd.SetActive(false);
        MenuGameEnd.SetActive(false);
        MenuLevel.SetActive(false);

        kManager = GameObject.Find("KeyManager").GetComponent<Key_Manager>();
    }

    void FixedUpdate()
    {

        if (kManager.reset)
        {
            Restart();
        }

        if (kManager.pause)
        {
            Pause();
        }

        if (dManager.GetIsDeath())
        {

            Death();
        }

        if (portalEnd.GetIsEnd())
        {
            End();
        }

        float time = chrono.GetTimer();

        int seconds = (int)time % 60;
        int minutes = (int)((time / 60) % 60);      

        if(lManager.GetIndex() != 0)
        {
            if(seconds <= 9){
                Timer.text = " " + minutes + " : 0" + seconds;
            } else {
            Timer.text = " " + minutes + " : " + seconds;
            }
        }
        else
        {
            Timer.text = "Tutorial";
        }
        
    }

    void Pause()
    {
        MenuPause.SetActive(true);
        Time.timeScale = 0f;
        
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        chrono.PauseChrono();
        
    }

    void Death()
    {
        if (callDead) return;
        callDead = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        tManager.startDeathAnim();
        StartCoroutine(WaitForDeath());
    }
    IEnumerator WaitForDeath(){
        
        yield return new WaitForSeconds(1.5f);
        MenuDeath.SetActive(true);
        Time.timeScale = 0f;
    }

    void End()
    {
        if (lManager.GetIndex() == 3)
            MenuGameEnd.SetActive(true);
        else
            MenuEnd.SetActive(true);
        Time.timeScale = 0f;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        chrono.EndChrono();
        save.SaveLevel(lManager.GetIndexNext());
    }

    public void Restart()
    {
        callDead = false;
        MenuDeath.SetActive(false);
        Time.timeScale = 1f;
        lManager.Restart();
        dManager.SetLive();
        tManager.Restart();
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Resume()
    {
        Time.timeScale = 1.0f;
        MenuPause.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        chrono.ResumeChrono();
    }

    public void Next()
    {
        MenuEnd.SetActive(false);
        MenuGameEnd.SetActive(false);
        Time.timeScale = 1.0f;
        lManager.NextLevel();
        portalEnd.IsNotEnd();
        tManager.Restart();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ChangeLevel(int indexLevel)
    {
        if (indexLevel > 1 && save.GetHS()[indexLevel - 2] != 0 || indexLevel == 1 && save.GetTransf()[1] != 0)
        {
            MenuLevel.SetActive(false);
            Time.timeScale = 1.0f;
            tManager.Restart();
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            lManager.SetLevel(lManager.GetLevel()[indexLevel]);
        }
    }

    public void Quit()
    {
        //Save();
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

}
