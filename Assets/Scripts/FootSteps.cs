using UnityEngine;

public class FootSteps : MonoBehaviour
{
	[SerializeField] private AudioClip[] clips = default;

	private AudioSource audioSource = default;


	private void Start(){
		audioSource = GetComponent<AudioSource>();
	}

	private AudioClip GetRandomClip(){
		return clips[UnityEngine.Random.Range(0, clips.Length)];
	}

	private void Step(){
		AudioClip clip = GetRandomClip();
		audioSource.PlayOneShot(clip);

	}
}
