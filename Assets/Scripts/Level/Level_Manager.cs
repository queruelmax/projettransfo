using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Manager : MonoBehaviour
{
    [SerializeField] private List<Level> levels = new List<Level>();
    [SerializeField] private Transform levelParent;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private GameObject portal;
    [SerializeField] private Chrono chrono;
    [SerializeField] private Save save;
    [SerializeField] private Transforms_Manager tManager;
    [SerializeField] private Key_Manager kManager;
    [SerializeField] private Death_Manager dManager;


    private Level currentLevel;

    public void Start()
    {
        SetLevel(levels[save.GetLevelId()]);
    }

    private void DestroyLevel()
    {
        int childCount = levelParent.childCount;
        for (int i = 0; i < childCount; i++)
        {
            Destroy(levelParent.GetChild(i).gameObject);
        }
        currentLevel = null;
    }

    public void SetLevel(Level level)
    {
        DestroyLevel();
        Instantiate(level.LevelPrefab, levelParent);
        dManager.SetLive();
        playerTransform.GetComponent<CharacterController>().enabled = false;
        tManager.GetTransforms().ForEach(t => t.cooldownActual = 0);
        playerTransform.position = level.StartPosition;
        playerTransform.rotation = Quaternion.identity;
        kManager.resetCam();
        playerTransform.GetComponent<Character_Movement>().setCam(level.Rotate.x, level.Rotate.y);
        playerTransform.GetComponent<CharacterController>().enabled = true;
        portal.transform.position = level.EndPosition;
        portal.transform.localScale = level.Scale;
        portal.GetComponent<Portal>().IsNotEnd();
        playerTransform.GetComponent<Character_Movement>().OnRestart();
        currentLevel = level;
        chrono.StartChrono(GetIndex());
    }

    public void Restart()
    {
        SetLevel(currentLevel);
    }

    public void NextLevel()
    {
        int index = levels.FindIndex(level => level.name == currentLevel.name);

        if (index == (levels.Count-1))
        {
            SetLevel(currentLevel);
        }
        else
        {
            
            SetLevel(levels[index+1]);
        }
    }

    public int GetIndex() { int index = levels.FindIndex(level => level.name == currentLevel.name); return index; }
    public int GetIndexNext() {
        int index = GetIndex();
        if (index == levels.Count - 1)
        {
            return index;
        }

        return (index + 1);
    }

    public List<Level> GetLevel() { return levels; }

}
