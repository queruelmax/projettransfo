using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Level", order = 1)]

public class Level : ScriptableObject
{

    [SerializeField] private Vector3 startPosition;
    public Vector3 StartPosition => startPosition;

    [SerializeField] private Vector3 endPosition;
    public Vector3 EndPosition => endPosition;

    [SerializeField] private GameObject levelPrefab;
    public GameObject LevelPrefab => levelPrefab;

    [SerializeField] private Vector3 scale;
    public Vector3 Scale => scale;

    [SerializeField] private Vector3 rotate;
    public Vector3 Rotate => rotate;

}
