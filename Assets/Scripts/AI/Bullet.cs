using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float destroyLimit = 20f;
    [SerializeField] private GameObject gameObject;
    
    private GameObject parent;

    private bool isClone;

    private Vector3 destination;

    private Death_Manager dManager;
    private Portal portal;

    void Start()
    {
        isClone = true;
        dManager = GameObject.Find("DeathManager").GetComponent<Death_Manager>();
        portal = GameObject.Find("EndPortal").GetComponent<Portal>();

    }

    void OnTriggerEnter(Collider coll)
    {
        if (isClone)
        {

            if (coll.CompareTag("Player"))
            {
                dManager.SetDeath();
            }

            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (gameObject == null || parent == null) return;
        if (Vector3.Distance(gameObject.transform.position, parent.transform.position) > destroyLimit)
        {
            Destroy(gameObject);
        }

        if (dManager.GetIsDeath())
        {
            Destroy(gameObject);
        }

        if (portal.GetIsEnd())
        {
            Destroy(gameObject);
        }
    }

    public void SetParent(GameObject prt) { parent = prt; }
}

