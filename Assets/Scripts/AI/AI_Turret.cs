using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Turret : MonoBehaviour
{

    [SerializeField] private float range;
    [SerializeField] private float speed;
    [SerializeField] private float cooldown;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject initPos;
    [SerializeField] private Material enableMat, disableMat;    

    private GameObject player;
    private bool isInRange;
    private bool isCooldown;
    private bool isActived = true;
    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("TargetPlayer");
        isInRange = false;
        InvokeRepeating("UpdateTarget", 0f, 0.1f);
        isCooldown = false;
    }

    void UpdateTarget()
    {
        
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distanceToPlayer <= range)
        {
            isInRange = true;
            transform.LookAt(player.transform);
        }
        else
        {
            isInRange=false;
        }
    }

    private void ThrowBall()
    {

        if (isInRange && !(isCooldown))
        {
            GameObject bulletClone = Instantiate(bullet, initPos.transform.position, initPos.transform.rotation);
            bulletClone.GetComponent<Rigidbody>().velocity = transform.forward * speed;
            bulletClone.GetComponent<Bullet>().SetParent(initPos);
            FindObjectOfType<AudioManager>().Play("TurretFire");
            isCooldown = true;
            StartCoroutine(wait());
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isActived)
            ThrowBall();
    }

    public void desactive(){
        foreach (Renderer r in transform.parent.GetComponentsInChildren<Renderer>()){
            r.material = disableMat;
        }
        isActived = false;
        StartCoroutine(waitdisable());
    }
    public void active(){
        foreach (Renderer r in transform.parent.GetComponentsInChildren<Renderer>()){
            r.material = enableMat;
        }
        isActived = true;
    }

    public IEnumerator waitdisable()
    {
        yield return new WaitForSeconds(20);
        active();
    }
    public IEnumerator wait()
    {
        yield return new WaitForSeconds(cooldown);
        isCooldown = false;
    }
}
