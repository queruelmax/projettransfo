using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transforms_Trigger : MonoBehaviour
{
    [SerializeField] private GameObject gameObject;
    private bool isTrigger;

    private Character_Movement characterMovement;
    private Transforms_Manager tManager;
    private bool chargAnim = false;

    public void Start()
    {
        GameObject managerGO = GameObject.Find("Player");
        characterMovement = managerGO.GetComponent<Character_Movement>();
        managerGO = GameObject.Find("TransformManager");
        tManager = managerGO.GetComponent<Transforms_Manager>();;

    }

    //Active the trigger Event
    void OnTriggerEnter(Collider collider)
    {
        if (collider.name == "Player")
        {
            isTrigger = true;
        }
    }
    //Active the trigger Event
    void OnTriggerExit(Collider collider)
    {
        if (collider.name == "Player")
        {
            isTrigger = false;
        }
    }

    void Update()
    {
        if (!isTrigger) return;
        Transforms currentTransforms = tManager.getCurrentTransforms();
        if (currentTransforms != null && currentTransforms.getName() == "Dig"){
            if (chargAnim && characterMovement.getAnimationTickDig() == 0){
                Destroy(gameObject);
            }
            chargAnim = characterMovement.getAnimationTickDig() > 0;
        }
    }
}
