using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Transforms : MonoBehaviour
{

    //Attributes Script
    private Key_Manager kManager;
    private Save save;

    //Attributes
    [SerializeField] private string name;

    [SerializeField] private int keyNumber;

    [SerializeField] private bool isActive;

    [SerializeField] private float cooldown;
    
    [SerializeField] public float cooldownActual;

    [SerializeField] private bool isLock;

    [SerializeField] private Texture textureTransform;

    private bool isKey;
    
    [SerializeField] private Image filler;
    public Image Filler => filler;
    [SerializeField] private GameObject highLight;
    public GameObject HighLight => highLight;
    [SerializeField] private GameObject picture;
    public GameObject Picture => picture;


    void Awake()
    {
        save = GameObject.Find("SaveManager").GetComponent<Save>();
        kManager = GameObject.Find("KeyManager").GetComponent<Key_Manager>();
        isLock = true;
    }

    void Start()
    {
        Picture.SetActive(!isLock);
    }

    void FixedUpdate()
    {
        isKey = kManager.GetKey(keyNumber);
    }

    //Getter
    public string getName() { return name; }
    public float getCooldown() { return cooldown;}
    public bool getIsLock() { return isLock;}
    public bool getIsKey() { return isKey; }
    public Texture getTextureTransform() { return textureTransform; }
    public void active(){
        isActive = true;
        HighLight.SetActive(true);
    }

    public void disable(){
        isActive = false;
        cooldownActual = getCooldown();
        HighLight.SetActive(false);
    }

    public void reload()
    {
        isActive = false;
        cooldownActual = 0;
        HighLight.SetActive(false);
    }

    public void UnlockTransforms(int x)
    {
        Picture.SetActive(true);
        isLock = false;
        save.SaveLock(x);
    }

    public void updateCooldown(bool isCurrent){
        cooldownActual -= Time.deltaTime;
        if (cooldownActual < 0.0f) {
            cooldownActual = 0;
        }
        if (Filler != null)
        {
           Filler.fillAmount = cooldownActual / (isCurrent ? 1 : getCooldown());
        }
    }
}
