using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTransforEnd : MonoBehaviour
{
    private float lifeTime = 2;
    private float rayon = 10;
    void Start()
    {
        GetInactiveInRadius();
    }
    void GetInactiveInRadius(){
        foreach (GameObject tr in FindObjectsOfType(typeof(GameObject))){
            if (tr.tag.Contains("IA")){
                float distanceSqr = (transform.position - tr.transform.position).sqrMagnitude;
                if(distanceSqr < rayon) {
                    tr.GetComponentInChildren<AI_Turret>().desactive();
                }
            }
        }
    }

    
    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0){
            Destroy(gameObject);
        }
    }
}
