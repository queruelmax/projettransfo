using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transforms_Manager : MonoBehaviour
{
    //Attributes Script
    [SerializeField] private Key_Manager kManager;

    //Attributes
    [SerializeField] private List<Transforms> transforms = new List<Transforms>();
    [SerializeField] private Texture defaultTextureTransform;
    private Texture oldTextureTransform;
    private Transforms currentTransforms; 
    [SerializeField] private GameObject player;
    [SerializeField] private Character_Movement characterMovement;
    [SerializeField] private Camera_Collider camera_Collider;
    [SerializeField] private GameObject viseur;
    private float animationTick = 0;
    [SerializeField] private Material playerMaterial, playerEyesMaterial;
    [SerializeField] private Save save;
    [SerializeField] private Death_Manager deathManager;
	

    void Start()
    {
        currentTransforms = null;
        oldTextureTransform = defaultTextureTransform;
        playerMaterial.SetFloat("isDeath", 0);
        playerEyesMaterial.mainTextureOffset = new Vector2(0, 0);
        updateSkin();

        for (int i = 0; i < transforms.Count; i++)
        {
            if(save.GetTransf()[i] == 1)
            {
                transforms[i].UnlockTransforms(i);
            }
        }
    }

    void Update()
    {
        if (kManager.unlockAll)
        {
            int i = 0;
            foreach(Transforms t in transforms)
            {
                t.UnlockTransforms(i);
                i++;
            }
        }
        for (int i = 0;i < transforms.Count; i++)
        {
            if ((transforms[i].getIsKey()) && !(transforms[i].getIsLock()))
            {
                if (transforms[i].cooldownActual == 0) {
                    if (transforms[i] == currentTransforms)
                    {
                        currentTransforms = null;
                        transforms[i].disable();
                        oldTextureTransform = transforms[i].getTextureTransform();
                        updateSkin();
                        animationTick = 1f;
                    } else{
                        FindObjectOfType<AudioManager>().Play("Transform");
                        if (currentTransforms != null)
                        {
                            currentTransforms.disable();
                            oldTextureTransform = currentTransforms.getTextureTransform();
                        } else {
                            oldTextureTransform = defaultTextureTransform;
                        }
                        currentTransforms = transforms[i];
                        transforms[i].active();
                        updateSkin();
                        animationTick = 1f;
                        for (int j = 0; j < transforms.Count; j++)
                        {
                            if (transforms[j] != currentTransforms)
                            {
                                transforms[j].cooldownActual = Mathf.Max(transforms[j].cooldownActual, 1f);
                            } else {
                                transforms[j].cooldownActual = 1f;
                            }
                        }
                    }
                } 
            } 

            transforms[i].updateCooldown(currentTransforms == transforms[i]);
        }

        playerMaterial.SetFloat("Lerp", (1f - animationTick));
        if (animationTick > 0)
        {
            animationTick -= Time.deltaTime;
            if (animationTick <= 0)
                animationTick = 0;
            if (currentTransforms != null && currentTransforms.getName() == "Scale")
                characterMovement.gameObject.transform.localScale = new Vector3(1, 1, 1) * (characterMovement.scaleTransform * (1f - animationTick) + animationTick);
            else if (characterMovement.transform.localScale != new Vector3(1, 1, 1))
                characterMovement.gameObject.transform.localScale = new Vector3(1, 1, 1) * (characterMovement.scaleTransform * animationTick + (1f - animationTick));
            if (currentTransforms != null && currentTransforms.getName() == "Arm"){
                camera_Collider.setOffSetDistance((1f - animationTick));
                if (animationTick == 0){
                    viseur.SetActive(true);
                }
            } else if (camera_Collider.getOffSetDistance() != 0) {
                camera_Collider.setOffSetDistance(animationTick);
                viseur.SetActive(false);
            }
        }
    }
    private void updateSkin(){
        playerMaterial.SetTexture("Texture2D_A", oldTextureTransform);
        playerMaterial.SetTexture("Texture2D_B", currentTransforms != null ? currentTransforms.getTextureTransform() : defaultTextureTransform);
    }

    public void Restart()
    {
        if (currentTransforms != null)
        {
            foreach (Transforms t in transforms)
            {
                t.reload();
            }
        }
        currentTransforms = null;
        characterMovement.gameObject.transform.localScale = new Vector3(1, 1, 1);
        playerMaterial.SetFloat("isDeath", 0);
        playerEyesMaterial.mainTextureOffset = new Vector2(0, 0);
        camera_Collider.setOffSetDistance(0);
        updateSkin();
    }

    public Transforms getCurrentTransforms() { return currentTransforms; }
    public GameObject getPlayer() { return player; }
    public List<Transforms> GetTransforms() { return transforms; }

    public void startDeathAnim(){
        playerMaterial.SetFloat("isDeath", 1);
        oldTextureTransform = currentTransforms.getTextureTransform();
        animationTick = 0.8f;
        playerEyesMaterial.mainTextureOffset = new Vector2(0, 0.66f);
        updateSkin();
    }

}
