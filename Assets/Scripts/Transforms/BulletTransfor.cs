using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTransfor : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject endGame;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    void OnTriggerEnter(Collider c)
    {
        FindObjectOfType<AudioManager>().Play("Explosion");
        Destroy(gameObject);
        Instantiate(endGame, transform.position, transform.rotation);

    }
}
