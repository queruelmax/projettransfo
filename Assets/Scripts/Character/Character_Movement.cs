using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Movement : MonoBehaviour
{
    public static float groundedGravity = -0.05f;
    public static float idleAnimationMin = 10f;



    //Attributes Script
    [SerializeField] private Transforms_Manager tManager;
    [SerializeField] private Camera_Collider camera_Collider;
    [SerializeField] private Death_Manager dManager;
    [SerializeField] private Chrono chrono;
    [SerializeField] private float speed;
    [SerializeField] private float sprintSpeed;
    [SerializeField] public float scaleTransform;
    [SerializeField, Range(0, 10)] private float lookSpeedX = 2.0f;
    [SerializeField, Range(0, 10)] private float lookSpeedY = 2.0f;
    [SerializeField, Range(1, 180)] private float lookAngleLimitMax = 90.0f;
    [SerializeField, Range(1, 180)] private float lookAngleLimitMin = 90.0f;
    [SerializeField] private Transform head_transform;
    [SerializeField] private float maxJumpTime = 0.5f;
    [SerializeField] private float jumpHeight = 3f;

    [SerializeField] private float maxJumpTimeSmall = 0.25f;
    [SerializeField] private float jumpHeightSmall = 1f;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float bulletSpeed;

    private Key_Manager kManager;    
    private float idle = 0;
    private float rotationX = 0.0f, rotationYdx = 0.0f, rotationY = 0.0f, rotationXdx = 0.0f;
    private float animationTickDig = 0, animationTickArm = 0;
    private Animator characterAnimator;
    private static readonly int _VelocityZ = Animator.StringToHash("VelocityZ");
    private static readonly int _VelocityX = Animator.StringToHash("VelocityX");
    private static readonly int _isJumping = Animator.StringToHash("isJumping");
    private static readonly int _Rotate = Animator.StringToHash("Rotate");
    private Vector2 velocity = Vector2.zero;
    public Vector3 moveVector = Vector3.zero;
    private CharacterController characterController;
    private float gravity = -9.81f, gravitySmall = -9.81f;
    private float initialJumpVelocity, initialJumpVelocitySmall;
    private bool isJumping = false;

    void Awake()
    {
        kManager = GameObject.Find("KeyManager").GetComponent<Key_Manager>();
        characterController = GetComponent<CharacterController>();

        float timeToApex = maxJumpTime / 2f;
        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToApex, 2);
        initialJumpVelocity = (2 * jumpHeight) / timeToApex;
        timeToApex = maxJumpTimeSmall / 2f;
        gravitySmall = -(2 * jumpHeightSmall) / Mathf.Pow(timeToApex, 2);
        initialJumpVelocitySmall = (2 * jumpHeightSmall) / timeToApex;
    }

    public void OnRestart(){
        animationTickDig = 0;
        characterAnimator.SetBool(_isJumping, false);
    }
    
    void Start()
    {
        characterAnimator = tManager.getPlayer().GetComponent<Animator>();
    }

    void HandleJump(){
        if (kManager.jump && characterController.isGrounded && !isJumping){
            isJumping = true;
            float initJumpVelo = Mathf.Abs(transform.localScale.x - 1) < 0.01 ? initialJumpVelocity : initialJumpVelocitySmall;
            moveVector.y = initJumpVelo * .5f;
            FindObjectOfType<AudioManager>().Play("Jump");
        } else if (!kManager.jump && isJumping && characterController.isGrounded){
            isJumping = false;
        }
    }

    void HandleGravity()
    {
        float gravityC = Mathf.Abs(transform.localScale.x - 1) < 0.01 ? gravity : gravitySmall;
        bool isFalling = moveVector.y <= 0 || !kManager.jump;
        float fallMultiplier = 2.0f;
        if (characterController.isGrounded){
            moveVector.y = groundedGravity;
        } else if (isFalling){
            float previousY = moveVector.y;
            float newVelocityY = moveVector.y + (gravityC * Time.deltaTime * fallMultiplier);
            float nextVelo = (previousY + newVelocityY) / 2;
            moveVector.y = nextVelo;
        } else {
            float previousVelo = moveVector.y;
            float newVelocity = moveVector.y + gravityC * Time.deltaTime;
            float nextVelo = (previousVelo + newVelocity) / 2;
            moveVector.y = nextVelo;
        }
    }

    public void setCam(float a, float b)
    {
        rotationX = a;
        rotationY = b;
    }

    void MoveInput()
    {
        if (Time.timeScale == 0f) return;
        rotationYdx = (kManager.MouseX) * lookSpeedX;
        rotationY += rotationYdx;
        rotationXdx = (kManager.MouseY) * lookSpeedY;
        rotationX -= rotationXdx;
        velocity = Vector2.zero;
    
        if (kManager.forward)  velocity.x += 1f;
        if (kManager.backward) velocity.x -= 1f;
        if (kManager.left) velocity.y -= 1f;
        if (kManager.right) velocity.y += 1f;
        if (kManager.interaction){
            if (animationTickDig == 0 && tManager.getCurrentTransforms() != null && tManager.getCurrentTransforms().getName() == "Dig")
            {
                animationTickDig = 0.6f;
                tManager.getPlayer().GetComponent<Animator>().Play("Dig");
            }
            if (animationTickArm == 0 && tManager.getCurrentTransforms() != null && tManager.getCurrentTransforms().getName() == "Arm")
            {
                animationTickArm = 0.6f;
                launchBullet();
                tManager.getPlayer().GetComponent<Animator>().Play("Arm");
            }
        }
        
        bool isSprinting = tManager.getCurrentTransforms() != null && tManager.getCurrentTransforms().getName() == "Speed";
        
        if (velocity != Vector2.zero){
            Vector2 moveVector2 = velocity.normalized * (speed + (isSprinting ? sprintSpeed : 0)) * transform.localScale.x;
            moveVector.x = moveVector2.y;
            moveVector.z = moveVector2.x;
            if (isSprinting)
                velocity *= 2;
            if (Mathf.Abs(rotationXdx) > 0.1 || Mathf.Abs(rotationYdx) > 0.1)
                idle = 0;   
        } else {
            moveVector.x = 0;
            moveVector.z = 0;
        }
    }
    
    void Update(){
        HandleJump();
        MoveInput();
        runAnimation();
        transform.rotation = Quaternion.Euler(0, rotationY, 0);
        characterController.Move(transform.TransformDirection(moveVector) * Time.deltaTime);
        HandleGravity();
    }

    void LateUpdate(){
        rotationX = Mathf.Clamp(rotationX, -lookAngleLimitMin, lookAngleLimitMax);
        head_transform.eulerAngles = new Vector3(rotationX, head_transform.eulerAngles.y, 0);
    }

    void runAnimation()
    { 
        idle += Time.deltaTime;
        if (idle > idleAnimationMin){
            idle = -10;
            characterAnimator.Play("IdleTooLong");
        }
        if (animationTickDig > 0){
            animationTickDig -= Time.deltaTime;
            if (animationTickDig <= 0){
                animationTickDig = 0;
            }
        }
        if (animationTickArm > 0){
            animationTickArm -= Time.deltaTime;
            if (animationTickArm <= 0){
                animationTickArm = 0;
            }
        }
        characterAnimator.SetBool(_isJumping, isJumping && !characterController.isGrounded);
        characterAnimator.SetFloat(_VelocityX, velocity.y, 0.1f, Time.deltaTime);
        characterAnimator.SetFloat(_VelocityZ, velocity.x, 0.1f, Time.deltaTime);
        characterAnimator.SetFloat(_Rotate, rotationYdx == 0 ? 0 : rotationYdx > 0 ? 1 : -1, 0.1f, Time.deltaTime);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Death"))
        {
            dManager.SetDeath();
        }
        if (collider.CompareTag("End"))
        {
            chrono.EndChrono();
        }
    }

    void OnDrawGizmos()
    {
        Vector3 vector3 = camera_Collider.transform.position;
    
        Vector3 dire = camera_Collider.offSetHead.position + camera_Collider.player.TransformDirection(camera_Collider.offsetFinal).normalized * camera_Collider.offsetDistance;
        
        Vector3 target = camera_Collider.transform.position + camera_Collider.transform.forward * 20;
        if (Physics.Linecast(camera_Collider.transform.position, target, out RaycastHit hit, ~(1 << LayerMask.NameToLayer("PlayerLayer"))))
        {
            target = hit.point;
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(target, 0.2f);
        }
        Vector3 pos = (camera_Collider.player.position + camera_Collider.offSetHead.position * 3) / 4;

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(pos, 0.2f);
        Gizmos.DrawLine(pos, target);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(camera_Collider.offSetHead.position + camera_Collider.player.TransformDirection(camera_Collider.offsetFinal).normalized * camera_Collider.offsetDistance, 0.2f);
        Gizmos.DrawLine(camera_Collider.transform.position, camera_Collider.transform.position + camera_Collider.transform.forward * 20);   
    }
    
    private void launchBullet(){

        Vector3 vector3 = camera_Collider.transform.position;
    
        Vector3 dire = camera_Collider.offSetHead.position + camera_Collider.player.TransformDirection(camera_Collider.offsetFinal).normalized * camera_Collider.offsetDistance;
        
        Vector3 target = camera_Collider.transform.position + camera_Collider.transform.forward * 20;
        if (Physics.Linecast(camera_Collider.transform.position, target, out RaycastHit hit, ~(1 << LayerMask.NameToLayer("PlayerLayer"))))
        {
            target = hit.point;
        }
        Vector3 pos = (camera_Collider.player.position + camera_Collider.offSetHead.position * 3) / 4;
        
        FindObjectOfType<AudioManager>().Play("StarWars");
        GameObject bullet = Instantiate(bulletPrefab, pos, Quaternion.identity);
        bullet.GetComponent<Rigidbody>().AddForce((target - pos).normalized * bulletSpeed, ForceMode.Impulse);
    }
    
    public float getAnimationTickDig()
    {
        return animationTickDig;
    }

}
