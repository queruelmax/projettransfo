using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Menu_Home : MonoBehaviour
{

    [SerializeField] private Texture defaultTexture;
    [SerializeField] private Material matos, matosEyes;
    [SerializeField] private List<Texture> textures = new List<Texture>();
    
    private float animationTick = 0;
    private float timeSwitchTransfo = 0;
    private Texture old;
    private float animShader;
    private int oldA;
    private float tickEyes = 15, oldoffset;
    private float offSetdx = 1f/3f;

    void Awake(){
        old = defaultTexture;
        matos.SetTexture("Texture2D_A", old);
        matos.SetTexture("Texture2D_B", defaultTexture);
        matos.SetFloat("Lerp", 0);
        matosEyes.mainTextureOffset = new Vector2(0, 0);
        Time.timeScale = 1.0f;
    }

    void Update()
    {
        if (animationTick > 0){
        
            animationTick -= Time.deltaTime;
            if (animationTick <= 0){
                animationTick = 0;
            }
        }
        tickEyes -= Time.deltaTime;
        if (tickEyes <= 1){
            matosEyes.mainTextureOffset = new Vector2(oldoffset + offSetdx * (1 - tickEyes), 0);
            if (tickEyes <= 0){
                if (Mathf.Abs(matosEyes.mainTextureOffset.x - 1) < 0.1f){
                    matosEyes.mainTextureOffset = new Vector2(0, 0);
                }
                tickEyes = 15;
                oldoffset = matosEyes.mainTextureOffset.x;
            }
        }

        timeSwitchTransfo += Time.deltaTime;
        if (timeSwitchTransfo > 10f){
            int random = oldA;
            while (random == oldA){
                random = Random.Range(0, 4);
            }
            if (random == 0){
                matos.SetTexture("Texture2D_A", old);
                matos.SetTexture("Texture2D_B", defaultTexture);
                old = defaultTexture;
            } else{
                matos.SetTexture("Texture2D_A", old);
                matos.SetTexture("Texture2D_B", textures[random]);
                old = textures[random];
            }
            animShader = 1;
            timeSwitchTransfo = 0;
            oldA = random;
        }

        if (animShader > 0){
            animShader -= Time.deltaTime;
            if (animShader <= 0){
                animShader = 0;
            }
            matos.SetFloat("Lerp", 1 - animShader);
        }
        
    }

    public void RandomAnimation(){
        int random = Random.Range(0, 6) + 1;
        if (animationTick == 0){
            animationTick = 1;
            GetComponent<Animator>().Play("Anim "+random);
        }
    }
}
