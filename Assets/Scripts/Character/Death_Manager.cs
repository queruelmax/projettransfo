using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death_Manager : MonoBehaviour
{

    private bool isDeath;

    void Start()
    {
        isDeath = false;
    }

    public bool GetIsDeath() { return isDeath; }
    public void SetDeath() { isDeath = true; FindObjectOfType<AudioManager>().Play("PlayerDeath");}
    public void SetLive() { isDeath = false; }



}
