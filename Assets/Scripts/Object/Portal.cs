using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    private bool isEnd;

    void Start()
    {
        isEnd = false;
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            isEnd = true;
            FindObjectOfType<AudioManager>().Play("Victory");
        }
    }

    public bool GetIsEnd() { return isEnd; }
    public void IsNotEnd() { isEnd = false; }
}
