using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformText : MonoBehaviour
{
    private Save save;
    public int transformID;

    // Start is called before the first frame update
    void Start()
    {
        save = GameObject.Find("SaveManager").GetComponent<Save>();
    }

    // Update is called once per frame
    void Update()
    {
        if(save.GetTransf()[transformID] == 1)
        {
            Destroy(gameObject);
        }
    }
}
