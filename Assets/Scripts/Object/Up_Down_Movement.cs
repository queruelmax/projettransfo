using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Up_Down_Movement : MonoBehaviour
{

    bool up;
    float wait;

    void Start()
    {
        up = false;
        wait = 0.0f;
    }

    void Update()
    {

        if(wait >= 1.0f)
        {
            transform.position += new Vector3(0, up ? 0.05f : -0.05f, 0);
            up = !up;
            wait = 0.0f;

        }

        wait += Time.deltaTime;
    }
}
