using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainTransform : MonoBehaviour
{

    private Transforms_Manager tManager;
    [SerializeField] private int transNumber;


    // Start is called before the first frame update
    void Start()
    {
        tManager = GameObject.Find("TransformManager").GetComponent<Transforms_Manager>();

    }


    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider c)
    {
        tManager.GetTransforms()[transNumber].UnlockTransforms(transNumber);
        Destroy(gameObject);
        FindObjectOfType<AudioManager>().Play("NewSpell");
    }
}
