using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Save : MonoBehaviour
{
    [SerializeField] private float[] HS = new float[3];
    [SerializeField] private int[] Transf = new int[4];
    [SerializeField] private int levelId;

    void Awake()
    {
        for (int i = 0; i < HS.Length; i++) { HS[i] = 0.0f; }
        for (int i = 0; i < Transf.Length; i++) { Transf[i] = 0; }
        levelId = 0;
        JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString("Save"), this);
    }

    public void SaveLock(int i)
    {
        Transf[i] = 1;
        SavePrefs();
    }
    
    public void SavePrefs(){
        PlayerPrefs.SetString("Save", JsonUtility.ToJson(this));
    }

    public void SaveChrono(int i, float f)
    {
        int index = i - 1;
        if (index >= 0)
        {
            if (f < HS[index] || HS[index] == 0)
            {
                HS[index] = f;
                SavePrefs();
            }
        }
    }

    public void SaveLevel(int i)
    {
        levelId = i;
        SavePrefs();
    }

    public float[] GetHS() { return HS; }
    public int[] GetTransf() { return Transf; }
    public int GetLevelId() { return levelId; }
}
