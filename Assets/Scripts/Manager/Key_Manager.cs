using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Key_Manager : MonoBehaviour
{

    //Attibutes
    public bool forward;
    public bool backward;
    public bool right;
    public bool left;
    public bool jump;
    public bool pause;
    public bool interaction;

    public bool unlockAll;

    public float MouseX;
    public float MouseY;

    public bool reset;

    public bool[] transforms;

    private Controls controls;

    void OnEnable()
    {
        controls = new Controls();

        //UNLOCKALL
        unlockAll = false;

        //FORWARD
        forward = false;
        controls.Player.Forward.performed += ctx => Forward(true);
        controls.Player.Forward.canceled += ctx => Forward(false);

        //BACKWARD
        backward = false;
        controls.Player.Backward.performed += ctx => Backward(true);
        controls.Player.Backward.canceled += ctx => Backward(false);

        //RIGHT
        right = false;
        controls.Player.Right.performed += ctx => Right(true);
        controls.Player.Right.canceled += ctx => Right(false);

        //LEFT
        left = false;
        controls.Player.Left.performed += ctx => Left(true);
        controls.Player.Left.canceled += ctx => Left(false);

        //JUMP
        jump = false;
        controls.Player.Jump.performed += ctx => Jump(true);
        controls.Player.Jump.canceled += ctx => Jump(false);

        //PAUSE
        pause = false;
        controls.Player.Pause.performed += ctx => Pause(true);
        controls.Player.Pause.canceled += ctx => Pause(false);

        //INTERACTION
        interaction = false;
        controls.Transforms.Interaction.performed += ctx => Interaction(true);
        controls.Transforms.Interaction.canceled += ctx => Interaction(false);

        //RESET
        reset = false;
        controls.Player.Reset.performed += ctx => Reset(true);
        controls.Player.Reset.canceled += ctx => Reset(false);

        transforms = new bool[4];

        for(int i = 0; i < transforms.Length; i++) { transforms[i] = false; }

        //FIRST
        controls.Transforms.FirstTransforms.performed += ctx => TransformsUpdate(true, 0);
        controls.Transforms.FirstTransforms.canceled += ctx => TransformsUpdate(false, 0);

        //SECOND
        controls.Transforms.SecondTransforms.performed += ctx => TransformsUpdate(true, 1);
        controls.Transforms.SecondTransforms.canceled += ctx => TransformsUpdate(false, 1);

        //THIRD
        controls.Transforms.ThirdTransforms.performed += ctx => TransformsUpdate(true, 2);
        controls.Transforms.ThirdTransforms.canceled += ctx => TransformsUpdate(false, 2);

        //FOURTH
        controls.Transforms.FourthTransforms.performed += ctx => TransformsUpdate(true, 3);
        controls.Transforms.FourthTransforms.canceled += ctx => TransformsUpdate(false, 3);

        MouseX = 0;
        MouseY = 0;
        controls.Enable(); 
    }

    private void OnDisable() => controls.Disable();

    void Forward(bool b) { forward = b; }
    void Backward(bool b) { backward = b; }
    void Right(bool b) { right = b; }
    void Left(bool b) { left = b; }
    void Jump(bool b) { jump = b; }
    void Pause(bool b) { pause = b; }
    void Interaction(bool b) { interaction = b; }
    void TransformsUpdate(bool b, int x) { transforms[x] = b; }
    void Reset(bool b) { reset = b; }


    //Return key for transforms
    public bool GetKey(int keyNumber)
    {
        return transforms[keyNumber-1];
    }

    public void resetCam()
    {
        MouseY = 0;
        MouseX = 0;
    }

    void FixedUpdate()
    {
        resetCam();
        MouseX = Mouse.current.delta.x.ReadValue();
        MouseY = Mouse.current.delta.y.ReadValue();
    }
}
