using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene_Manager : MonoBehaviour
{

    [SerializeField] private Save save;

    public void LoadScene(string SceneToLoad)
    {
        SceneManager.LoadScene(SceneToLoad);  
    }

    public void setLevel(int level)
    {
        save.SaveLevel(level);

    }

    public void Quit()
    {
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        #else
                Application.Quit();
        #endif
    }
}
