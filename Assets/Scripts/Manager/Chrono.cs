using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chrono : MonoBehaviour
{

    private bool isRun;
    private float current;
    private int indexLevel;

    [SerializeField] private Save save;

    void Start()
    {
        isRun = false;
        indexLevel = 0;
        current = 0;
    }

    void Update()
    {
        if (isRun)
        {
            current += Time.deltaTime;
        }
    }

    public void StartChrono(int i)
    {
        indexLevel = i;
        current = 0;
        isRun = true;
    }

    public void PauseChrono()
    {
        isRun = false;
    }

    public void ResumeChrono()
    {
        isRun = true;
    }

    public void EndChrono()
    {
        isRun = false;
        save.SaveChrono(indexLevel,current);
    }

    public float GetTimer() { return current; }

}
