// GENERATED AUTOMATICALLY FROM 'Assets/Input/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Transforms"",
            ""id"": ""96ab4d2e-5854-4650-8411-a3df26009d95"",
            ""actions"": [
                {
                    ""name"": ""Interaction"",
                    ""type"": ""Button"",
                    ""id"": ""c1c8e7f9-4427-4dfe-bcb1-c7182339b092"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FirstTransforms"",
                    ""type"": ""Button"",
                    ""id"": ""f4a7f5aa-f08b-4d4a-801b-2dd74bc0a3f9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SecondTransforms"",
                    ""type"": ""Button"",
                    ""id"": ""46c8e909-fd0e-4eeb-893a-27d2f35352e1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ThirdTransforms"",
                    ""type"": ""Button"",
                    ""id"": ""c73d7728-e9e8-416d-ae53-2c55e33b3cd9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FourthTransforms"",
                    ""type"": ""Button"",
                    ""id"": ""0cfe48bf-5225-4708-9c3f-d9e5e21cf77e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""669aa321-a2c1-4e6b-9346-ad19e9d337c8"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aeaf59df-a816-4c60-8348-7f6604cfa6ed"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""FirstTransforms"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""990e0801-4d01-4289-a24a-8029be07a1b9"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""SecondTransforms"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b5ea9027-3a89-4aed-978e-85ef1353e2b4"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""ThirdTransforms"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""85d15711-2d74-42b3-961b-ef0393443d5b"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""FourthTransforms"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Player"",
            ""id"": ""fae21277-9dd4-4b44-850b-daa3158cceb8"",
            ""actions"": [
                {
                    ""name"": ""Forward"",
                    ""type"": ""Button"",
                    ""id"": ""4ac77cf3-14e3-4cbb-a65e-b683a5f88400"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Backward"",
                    ""type"": ""Button"",
                    ""id"": ""8bcb2f3a-fda2-43f9-8dbc-63d368cbb1d6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Left"",
                    ""type"": ""Button"",
                    ""id"": ""ce0dc038-a0d3-4a74-b6ec-27fe955c139d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right"",
                    ""type"": ""Button"",
                    ""id"": ""33ce089e-93cf-4f3a-a3f1-4ab13e6c18ec"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""02f88e8d-3246-4626-a2f3-1333c07da9cd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""85dd359d-c88a-4d31-8261-0787bc152820"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UnlockAll"",
                    ""type"": ""Button"",
                    ""id"": ""f2124d68-1fdc-444e-96c8-97488516b2e2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Reset"",
                    ""type"": ""Button"",
                    ""id"": ""8717a4df-436a-4176-bfac-24bc4e5438dc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2025467e-ddb1-41e3-868b-4012475e1cb6"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""Forward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""beecc850-4eb9-40d6-82bc-60ce2d72a19c"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""Backward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d667c72-3a6d-406e-a228-d3186ef4fa23"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c708fa09-1ce8-4fc4-98a5-fb2fe459ea58"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2751f294-1e4b-4faa-af20-452556a8b0ad"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""13e61301-787f-4879-afcd-61ad92189635"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4f6aaa43-4503-4330-8ba5-b7ebbd7d2aff"",
                    ""path"": ""<Keyboard>/u"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""UnlockAll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e5414fb4-b7d7-4d19-a8ac-7e23e5d04c7a"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Reset"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Mouse"",
            ""id"": ""b35415fc-3ba2-4f72-90e7-420db5818c6e"",
            ""actions"": [
                {
                    ""name"": ""MouseX"",
                    ""type"": ""Button"",
                    ""id"": ""fa556128-0c16-4027-83fb-1488f7ba73dd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MouseY"",
                    ""type"": ""Button"",
                    ""id"": ""d67bebf6-7129-41ee-9ae4-22c68e2ca7e2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a9dc75ac-870d-4fa5-ba9f-f54d1dd3a5f5"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""MouseX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""56ad601b-a863-4919-89d7-67b108724f6a"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoard & Mouse"",
                    ""action"": ""MouseY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyBoard & Mouse"",
            ""bindingGroup"": ""KeyBoard & Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Transforms
        m_Transforms = asset.FindActionMap("Transforms", throwIfNotFound: true);
        m_Transforms_Interaction = m_Transforms.FindAction("Interaction", throwIfNotFound: true);
        m_Transforms_FirstTransforms = m_Transforms.FindAction("FirstTransforms", throwIfNotFound: true);
        m_Transforms_SecondTransforms = m_Transforms.FindAction("SecondTransforms", throwIfNotFound: true);
        m_Transforms_ThirdTransforms = m_Transforms.FindAction("ThirdTransforms", throwIfNotFound: true);
        m_Transforms_FourthTransforms = m_Transforms.FindAction("FourthTransforms", throwIfNotFound: true);
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Forward = m_Player.FindAction("Forward", throwIfNotFound: true);
        m_Player_Backward = m_Player.FindAction("Backward", throwIfNotFound: true);
        m_Player_Left = m_Player.FindAction("Left", throwIfNotFound: true);
        m_Player_Right = m_Player.FindAction("Right", throwIfNotFound: true);
        m_Player_Jump = m_Player.FindAction("Jump", throwIfNotFound: true);
        m_Player_Pause = m_Player.FindAction("Pause", throwIfNotFound: true);
        m_Player_UnlockAll = m_Player.FindAction("UnlockAll", throwIfNotFound: true);
        m_Player_Reset = m_Player.FindAction("Reset", throwIfNotFound: true);
        // Mouse
        m_Mouse = asset.FindActionMap("Mouse", throwIfNotFound: true);
        m_Mouse_MouseX = m_Mouse.FindAction("MouseX", throwIfNotFound: true);
        m_Mouse_MouseY = m_Mouse.FindAction("MouseY", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Transforms
    private readonly InputActionMap m_Transforms;
    private ITransformsActions m_TransformsActionsCallbackInterface;
    private readonly InputAction m_Transforms_Interaction;
    private readonly InputAction m_Transforms_FirstTransforms;
    private readonly InputAction m_Transforms_SecondTransforms;
    private readonly InputAction m_Transforms_ThirdTransforms;
    private readonly InputAction m_Transforms_FourthTransforms;
    public struct TransformsActions
    {
        private @Controls m_Wrapper;
        public TransformsActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interaction => m_Wrapper.m_Transforms_Interaction;
        public InputAction @FirstTransforms => m_Wrapper.m_Transforms_FirstTransforms;
        public InputAction @SecondTransforms => m_Wrapper.m_Transforms_SecondTransforms;
        public InputAction @ThirdTransforms => m_Wrapper.m_Transforms_ThirdTransforms;
        public InputAction @FourthTransforms => m_Wrapper.m_Transforms_FourthTransforms;
        public InputActionMap Get() { return m_Wrapper.m_Transforms; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(TransformsActions set) { return set.Get(); }
        public void SetCallbacks(ITransformsActions instance)
        {
            if (m_Wrapper.m_TransformsActionsCallbackInterface != null)
            {
                @Interaction.started -= m_Wrapper.m_TransformsActionsCallbackInterface.OnInteraction;
                @Interaction.performed -= m_Wrapper.m_TransformsActionsCallbackInterface.OnInteraction;
                @Interaction.canceled -= m_Wrapper.m_TransformsActionsCallbackInterface.OnInteraction;
                @FirstTransforms.started -= m_Wrapper.m_TransformsActionsCallbackInterface.OnFirstTransforms;
                @FirstTransforms.performed -= m_Wrapper.m_TransformsActionsCallbackInterface.OnFirstTransforms;
                @FirstTransforms.canceled -= m_Wrapper.m_TransformsActionsCallbackInterface.OnFirstTransforms;
                @SecondTransforms.started -= m_Wrapper.m_TransformsActionsCallbackInterface.OnSecondTransforms;
                @SecondTransforms.performed -= m_Wrapper.m_TransformsActionsCallbackInterface.OnSecondTransforms;
                @SecondTransforms.canceled -= m_Wrapper.m_TransformsActionsCallbackInterface.OnSecondTransforms;
                @ThirdTransforms.started -= m_Wrapper.m_TransformsActionsCallbackInterface.OnThirdTransforms;
                @ThirdTransforms.performed -= m_Wrapper.m_TransformsActionsCallbackInterface.OnThirdTransforms;
                @ThirdTransforms.canceled -= m_Wrapper.m_TransformsActionsCallbackInterface.OnThirdTransforms;
                @FourthTransforms.started -= m_Wrapper.m_TransformsActionsCallbackInterface.OnFourthTransforms;
                @FourthTransforms.performed -= m_Wrapper.m_TransformsActionsCallbackInterface.OnFourthTransforms;
                @FourthTransforms.canceled -= m_Wrapper.m_TransformsActionsCallbackInterface.OnFourthTransforms;
            }
            m_Wrapper.m_TransformsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interaction.started += instance.OnInteraction;
                @Interaction.performed += instance.OnInteraction;
                @Interaction.canceled += instance.OnInteraction;
                @FirstTransforms.started += instance.OnFirstTransforms;
                @FirstTransforms.performed += instance.OnFirstTransforms;
                @FirstTransforms.canceled += instance.OnFirstTransforms;
                @SecondTransforms.started += instance.OnSecondTransforms;
                @SecondTransforms.performed += instance.OnSecondTransforms;
                @SecondTransforms.canceled += instance.OnSecondTransforms;
                @ThirdTransforms.started += instance.OnThirdTransforms;
                @ThirdTransforms.performed += instance.OnThirdTransforms;
                @ThirdTransforms.canceled += instance.OnThirdTransforms;
                @FourthTransforms.started += instance.OnFourthTransforms;
                @FourthTransforms.performed += instance.OnFourthTransforms;
                @FourthTransforms.canceled += instance.OnFourthTransforms;
            }
        }
    }
    public TransformsActions @Transforms => new TransformsActions(this);

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Forward;
    private readonly InputAction m_Player_Backward;
    private readonly InputAction m_Player_Left;
    private readonly InputAction m_Player_Right;
    private readonly InputAction m_Player_Jump;
    private readonly InputAction m_Player_Pause;
    private readonly InputAction m_Player_UnlockAll;
    private readonly InputAction m_Player_Reset;
    public struct PlayerActions
    {
        private @Controls m_Wrapper;
        public PlayerActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Forward => m_Wrapper.m_Player_Forward;
        public InputAction @Backward => m_Wrapper.m_Player_Backward;
        public InputAction @Left => m_Wrapper.m_Player_Left;
        public InputAction @Right => m_Wrapper.m_Player_Right;
        public InputAction @Jump => m_Wrapper.m_Player_Jump;
        public InputAction @Pause => m_Wrapper.m_Player_Pause;
        public InputAction @UnlockAll => m_Wrapper.m_Player_UnlockAll;
        public InputAction @Reset => m_Wrapper.m_Player_Reset;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Forward.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnForward;
                @Forward.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnForward;
                @Forward.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnForward;
                @Backward.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBackward;
                @Backward.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBackward;
                @Backward.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBackward;
                @Left.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeft;
                @Left.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeft;
                @Left.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeft;
                @Right.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRight;
                @Right.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRight;
                @Right.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRight;
                @Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Pause.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                @UnlockAll.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnUnlockAll;
                @UnlockAll.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnUnlockAll;
                @UnlockAll.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnUnlockAll;
                @Reset.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReset;
                @Reset.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReset;
                @Reset.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReset;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Forward.started += instance.OnForward;
                @Forward.performed += instance.OnForward;
                @Forward.canceled += instance.OnForward;
                @Backward.started += instance.OnBackward;
                @Backward.performed += instance.OnBackward;
                @Backward.canceled += instance.OnBackward;
                @Left.started += instance.OnLeft;
                @Left.performed += instance.OnLeft;
                @Left.canceled += instance.OnLeft;
                @Right.started += instance.OnRight;
                @Right.performed += instance.OnRight;
                @Right.canceled += instance.OnRight;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
                @UnlockAll.started += instance.OnUnlockAll;
                @UnlockAll.performed += instance.OnUnlockAll;
                @UnlockAll.canceled += instance.OnUnlockAll;
                @Reset.started += instance.OnReset;
                @Reset.performed += instance.OnReset;
                @Reset.canceled += instance.OnReset;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);

    // Mouse
    private readonly InputActionMap m_Mouse;
    private IMouseActions m_MouseActionsCallbackInterface;
    private readonly InputAction m_Mouse_MouseX;
    private readonly InputAction m_Mouse_MouseY;
    public struct MouseActions
    {
        private @Controls m_Wrapper;
        public MouseActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @MouseX => m_Wrapper.m_Mouse_MouseX;
        public InputAction @MouseY => m_Wrapper.m_Mouse_MouseY;
        public InputActionMap Get() { return m_Wrapper.m_Mouse; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouseActions set) { return set.Get(); }
        public void SetCallbacks(IMouseActions instance)
        {
            if (m_Wrapper.m_MouseActionsCallbackInterface != null)
            {
                @MouseX.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnMouseX;
                @MouseX.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnMouseX;
                @MouseX.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnMouseX;
                @MouseY.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnMouseY;
                @MouseY.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnMouseY;
                @MouseY.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnMouseY;
            }
            m_Wrapper.m_MouseActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MouseX.started += instance.OnMouseX;
                @MouseX.performed += instance.OnMouseX;
                @MouseX.canceled += instance.OnMouseX;
                @MouseY.started += instance.OnMouseY;
                @MouseY.performed += instance.OnMouseY;
                @MouseY.canceled += instance.OnMouseY;
            }
        }
    }
    public MouseActions @Mouse => new MouseActions(this);
    private int m_KeyBoardMouseSchemeIndex = -1;
    public InputControlScheme KeyBoardMouseScheme
    {
        get
        {
            if (m_KeyBoardMouseSchemeIndex == -1) m_KeyBoardMouseSchemeIndex = asset.FindControlSchemeIndex("KeyBoard & Mouse");
            return asset.controlSchemes[m_KeyBoardMouseSchemeIndex];
        }
    }
    public interface ITransformsActions
    {
        void OnInteraction(InputAction.CallbackContext context);
        void OnFirstTransforms(InputAction.CallbackContext context);
        void OnSecondTransforms(InputAction.CallbackContext context);
        void OnThirdTransforms(InputAction.CallbackContext context);
        void OnFourthTransforms(InputAction.CallbackContext context);
    }
    public interface IPlayerActions
    {
        void OnForward(InputAction.CallbackContext context);
        void OnBackward(InputAction.CallbackContext context);
        void OnLeft(InputAction.CallbackContext context);
        void OnRight(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
        void OnUnlockAll(InputAction.CallbackContext context);
        void OnReset(InputAction.CallbackContext context);
    }
    public interface IMouseActions
    {
        void OnMouseX(InputAction.CallbackContext context);
        void OnMouseY(InputAction.CallbackContext context);
    }
}
